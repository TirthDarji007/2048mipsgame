# You can play 2048 using this main


# Constants
.data
buffer: .space 20
string_ask_move: .asciiz "Enter the move: "
string_ask_row: .asciiz "Enter the row value: "
string_ask_col: .asciiz "Enter the col value: "
string_ask_val: .asciiz "Enter the value: "
string_newline: .asciiz "\n"


.macro print_int (%x)
li $v0, 1
add $a0, $zero, %x
syscall
.end_macro

.macro exit_prog()
li $v0, 10
syscall
.end_macro

.macro print_newline()
la $a0, string_newline
li $v0, 4
syscall
.end_macro

.text
.globl _start


####################################################################
# This is the "main" of the program; Everything starts here.
####################################################################

_start:

	li $s0, 0xffff0000	# base 
	li $s1, 2	# n
	li $s2, 2	# m
	
	# start_game testing
	move $a0, $s0
	move $a1, $s1	# n
	move $a2, $s2	# m
	li $a3, 0	# r1
	li $t0, 0	# c1
	li $t1, 0	# r2
	li $t2, 1	# c2
	addi $sp, $sp, -12
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t2, 8($sp)
	jal start_game
	addi $sp, $sp, 12
	move $t0, $v0
	print_int($t0)
	print_newline
	
	loop_user_moves:
		# ask for move
		la $a0, string_ask_move
		li $v0, 4
		syscall
		# ask to enter move
		li $v0, 8
		la $a0, buffer
		li $a1, 2
		syscall
		print_newline
		#print the user input 
		#la $a0, buffer
		#li $v0, 4
		#syscall
		
		# user wants to place value
		li $t0, 'P'
		lw $t1, buffer
		beq $t0, $t1, Ask_to_place_value
		
		# user wants to exit
		li $t0, 'E'
		lw $t1, buffer
		beq $t0, $t1, exit_hw4
		
		# make a move as per user
		move $a0, $s0
		move $a1, $s1	# n
		move $a2, $s2	# m
		lw $a3, buffer 	# dir 
		jal user_move
		move $t0, $v0
		move $t1, $v1
		add $t2, $v0, $v1
		print_int($t0)
		print_int($t1)
		print_newline
		li $t0, 1
		beq $t1, $t0, exit_hw4		# If game won, exit
		li $t0, -1
		beq $t2, $t0, exit_hw4		# If game lost, exit
		
		# loop 
		j loop_user_moves
		
	Ask_to_place_value:
		# ask for row
		la $a0, string_ask_row
		li $v0, 4
		syscall
		# read the row
		li $v0, 5
		syscall
		move $s3, $v0			# s3 = row 
		# ask for col
		la $a0, string_ask_col
		li $v0, 4
		syscall
		# read the col
		li $v0, 5
		syscall
		move $s4, $v0			# s4 = col
		# ask for value
		la $a0, string_ask_val
		li $v0, 4
		syscall
		# read the value 
		li $v0, 5
		syscall
		move $s5, $v0			# s5 = val
		
		# call the place
		move $a0, $s0
		move $a1, $s1	# n
		move $a2, $s2	# m
		move $a3, $s3	# row, i
		move $t0, $s4	# col, j
		move $t1, $s5	# val
		addi $sp, $sp, -8
		sw $t0, 0($sp)
		sw $t1, 4($sp)
		jal place
		addi $sp, $sp, 8
		move $t0, $v0
		move $t1, $v1
		print_int($t0)
		print_newline
		
		# loop back
		j loop_user_moves
		
	exit_hw4:
	# Exit the program
	exit_prog()

###################################################################
# End of MAIN program
####################################################################


.include "hw4.asm"
