
##############################################################
# Name: Tirth Darji
##############################################################
.text

# For HW4, n means rows and m means columns with n and m ? 2.

##############################
# PART 1 FUNCTIONS
##############################


clear_board:
    #int clear_board(cell[][] board, int num_rows, int num_cols)
    # a0 = base_addr, a1 = n, a2 = m
	############################################
	
	# Preserve the Data
	addi $sp, $sp, -16			
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $ra, 12($sp)	
	# store arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	
	# call validate function
	move $a0, $s1
	move $a1, $s2
	li $a2, 0
	li $a3, 0
	jal validate
	li $t0, -1
	beq $v0, $t0, error_clear_board		# error in n and m 
	
	# start the reset register value loop
	move $t0, $s0		# current address = starting address
	mul $t1, $s1, $s2	# n * m
	sll $t1, $t1, 1		# n * m * 2
	li $t2, 2
	sub $t1, $t1, $t2	# ( n * m * 2 ) - 2
	add $t1, $t0, $t1	# last address inclusive , base_addr + ( n * m * 2 ) - 2
	li $t2, -1
	
	loop_clear_board:
		bgt $t0, $t1, endloop_clear_board
		sh $t2, 0($t0)
		addi $t0, $t0,2
		j loop_clear_board		
	endloop_clear_board:
	
	li $v0, 0
	j exit_clear_board
	
	error_clear_board:
		li $v0, -1
	
	exit_clear_board:
		# Unpreserve the Data			
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $ra, 12($sp)
		addi $sp, $sp, 16
		jr $ra
	############################################	

place:
    #int place(int[][] board, int n_rows, int n_cols, int row, int col,int val)
	############################################
	
	lw $t0, 0($sp)		# col
	lw $t1, 4($sp)		# val
	# Preserve the Data
	addi $sp, $sp, -28			
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	# store arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	move $s3, $a3		# row		
	move $s4, $t0		# col
	move $s5, $t1		# val
	
	# call validate function to check n,m,i,j
	move $a0, $s1
	move $a1, $s2
	move $a2, $s3
	move $a3, $s4
	jal validate
	li $t0, -1
	beq $v0, $t0, error_place
	
	# check for valid value $s5 = val
	li $t0, -1
	blt $s5, $t0, error_place		# val < -1
	beq $s5, $t0, valid_value_place		# val = -1, its valid
	beqz $s5, error_place			# val = 0
	li $t0, 1
	beq $s5, $t0, error_place		# val = 1
	sub $t0, $s5, $t0			# t0 = val - 1
	and $t0, $s5, $t0			# f = (v & (v - 1)) == 0; for power of 2
	bnez $t0, error_place
	valid_value_place:
	
	# get the address of cell
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	move $a2, $s3			# row, i
	move $a3, $s4			# col, j
	jal get_cell_addr
	# $vo has the valid address
	sh $s5, 0($v0)			# store val at addr[i][j]
	li $v0, 0
	j exit_place
	
	error_place:
		li $v0, -1
	
	exit_place:
		# Unpreserve the Data		
		lw $ra, 0($sp)
		lw $s0, 4($sp)
		lw $s1, 8($sp)
		lw $s2, 12($sp)
		lw $s3, 16($sp)
		lw $s4, 20($sp)
		lw $s5, 24($sp)
		addi $sp, $sp, 28		
		jr $ra	
	
	############################################

start_game:
    #int start_game(cell[][] board, int num_rows, int num_cols,int r1, int c1, int r2, int c2)
	############################################
	lw $t0, 0($sp)		# c1
	lw $t1, 4($sp)		# r2
	lw $t2, 8($sp)		# c2
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	move $s3, $a3		# r1		
	move $s4, $t0		# c1
	move $s5, $t1		# r2
	move $s6, $t2		# c2
	
	# validate n,m,r1,c1,r2,c2
	move $a0, $s1		# n
	move $a1, $s2		# m
	move $a2, $s3		# r1
	move $a3, $s4		# c1
	jal validate	
	bnez $v0, error_start_game
	move $a2, $s5		# r2
	move $a3, $s6		# c2
	jal validate	
	bnez $v0, error_start_game
	
	# call clear_board
	move $a0, $s0		# basr_addr
	move $a1, $s1		# n
	move $a2, $s2		# m
	jal clear_board
	bnez $v0, error_start_game			# error in clear_board
	
	# put value at r1,c1
	li $t0, 2
	move $a0, $s0		# base_addr
	move $a1, $s1		# n
	move $a2, $s2		# m
	move $a3, $s3		# r1
	addi $sp, $sp, -8
	sw $s4, 0($sp)		# c1
	sw $t0, 4($sp)
	jal place
	addi $sp, $sp, 8
	bnez $v0, error_start_game
	
	# put value at r2,c2
	li $t0, 2
	move $a0, $s0		# base_addr
	move $a1, $s1		# n
	move $a2, $s2		# m
	move $a3, $s5		# r2
	addi $sp, $sp, -8
	sw $s6, 0($sp)		# c2
	sw $t0, 4($sp)
	jal place
	addi $sp, $sp, 8
	bnez $v0, error_start_game
	
	li $v0, 0
	j exit_start_game
	
	error_start_game:
		li $v0, -1
	exit_start_game:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra
	############################################

##############################
# PART 2 FUNCTIONS
##############################

merge_row:
    #int merge_row(cell[][] board, int num_rows, int num_cols, int row, int direction)
	############################################
	
	lw $t0, 0($sp)		# direction
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	move $s3, $a3		# row		
	move $s4, $t0		# direction
	
	# validate n,m,row 
	move $a0, $s1		# n
	move $a1, $s2		# m
	move $a2, $s3		# row
	li $a3, 0		# col
	jal validate
	bnez $v0, error_merge_row
	
	# validate direction value, $s4, valid value: 0,1
	bltz $s4, error_merge_row	# direction < 0
	li $t0, 1
	bgt $s4, $t0, error_merge_row	# direction > 1
	
	# get the begining address of row
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	move $a2, $s3			# row, i
	li $a3, 0			# col, j
	jal get_cell_addr
	move $s5, $v0			# s5 has starting adress of row
	# get last address of row
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	move $a2, $s3			# row, i
	addi $a3, $s2, -1		# j = m -1 
	jal get_cell_addr
	move $s6, $v0			# s6 has ending adress of row
	
	# save staring and end address before looping in any direction
	move $s0, $s5			# start address 
	move $s1, $s6			# end address 
	
	bnez $s4, direction_1_merge_row
	# direction value is 0
	addi $s7, $s5, 2		# adjent cell address
	dir_0_loop_merge_row:
		bgt $s7, $s6, end_dir_0_loop_merge_row		# adjent cell address > ending address
		move $a0, $s5
		move $a1, $s7
		jal merge
		addi $s5, $s5, 2
		addi $s7, $s7, 2
		j dir_0_loop_merge_row
	end_dir_0_loop_merge_row:
	j count_val_merge_row_base
	
	# jump here when direction is one	
	direction_1_merge_row:
	addi $s7, $s6, -2		# adjent cell address
		dir_1_loop_merge_row:
			blt $s7, $s5, end_dir_1_loop_merge_row	# adj address < start address
			move $a0, $s6
			move $a1, $s7
			jal merge
			addi $s6, $s6, -2
			addi $s7, $s7, -2
			j dir_1_loop_merge_row
		end_dir_1_loop_merge_row:
	# merging row is finished
	 count_val_merge_row_base:
	li $s7, 0		# non emply value counter , s0 = start, s1 = end
	count_val_merge_row:
		bgt $s0, $s1, end_count_val_merge_row	# start > end
		lh $t0, 0($s0)
		blez $t0, iinc_count_val_merge_row	# value <= 0
		addi $s7, $s7, 1
		iinc_count_val_merge_row:
			addi $s0, $s0, 2
		j count_val_merge_row
	end_count_val_merge_row:
	
	move $v0, $s7
	j exit_merge_row
	
	error_merge_row:
		li $v0, -1
	exit_merge_row:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra
	############################################

merge_col:
    #int merge_col(cell[][] board, int num_rows, int num_cols, int col, int direction)
	############################################
    	lw $t0, 0($sp)		# direction
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	move $s3, $a3		# col		
	move $s4, $t0		# direction
	
	# validate n,m,col
	move $a0, $s1		# n
	move $a1, $s2		# m
	li  $a2, 0		# row
	move $a3, $s3		# col
	jal validate
	bnez $v0, error_merge_col
	
	# validate direction value, $s4, valid value: 0,1
	bltz $s4, error_merge_col	# direction < 0
	li $t0, 1
	bgt $s4, $t0, error_merge_col	# direction > 1
	
	# get the begining address of col
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	li  $a2, 0			# row, i
	move $a3, $s3			# col, j
	jal get_cell_addr
	move $s5, $v0			# s5 has starting adress of row
	# get last address of col
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	addi $a2, $s1, -1		# row= n -1
	move $a3, $s3			# col
	jal get_cell_addr
	move $s6, $v0			# s6 has ending adress of row
	
	# save staring and end address before looping in any direction
	move $s0, $s5			# start address 
	move $s1, $s6			# end address 
	
	bnez $s4, direction_1_merge_col
	# direction value is 0,  bottom to top, S5= start, S6=stop
	sub $s4, $0, $s2		# s4 = -m
	sll $s4, $s4, 1
	add $s7, $s6, $s4		# s7 = adj col cell 
	dir_0_loop_merge_col:
		blt $s7, $s5, end_dir_0_loop_merge_col		# adjent cell address < start address
		move $a0, $s6
		move $a1, $s7
		jal merge
		add $s6, $s6, $s4
		add $s7, $s7, $s4
		j dir_0_loop_merge_col
	end_dir_0_loop_merge_col:
	j count_val_merge_col_base
	
	direction_1_merge_col:
    		# top to bottom, s5 = start, s6 = end
    		sll $s4, $s2, 1
    		add $s7, $s5, $s4		# adjent cell address
		dir_1_loop_merge_col:
			bgt $s7, $s6, end_dir_1_loop_merge_col	# adj address > end address
			move $a0, $s5
			move $a1, $s7
			jal merge
			add $s5, $s5, $s4
			add $s7, $s7, $s4
			j dir_1_loop_merge_col
		end_dir_1_loop_merge_col:
	# merging row is finished
    		
    	count_val_merge_col_base:
    	li $s7, 0		# non emply value counter , s0 = start, s1 = end
    	sll $s2, $s2, 1		# s2 = 2m
	count_val_merge_col:
		bgt $s0, $s1, end_count_val_merge_col	# start > end
		lh $t0, 0($s0)
		blez $t0, iinc_count_val_merge_col	# value <= 0
		addi $s7, $s7, 1
		iinc_count_val_merge_col:
			add $s0, $s0, $s2
		j count_val_merge_col
	end_count_val_merge_col:
	
	move $v0, $s7
	j exit_merge_col
    	
    	error_merge_col:
    		li $v0, -1
    	exit_merge_col:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra    	
    	###########################################

shift_row:
    #int shift_row(cell[][] board, int num_rows, int num_cols, int row, int direction)
	############################################
	lw $t0, 0($sp)		# direction
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	move $s3, $a3		# row		
	move $s4, $t0		# direction
	
	# validate n,m,row 
	move $a0, $s1		# n
	move $a1, $s2		# m
	move $a2, $s3		# row
	li $a3, 0		# col
	jal validate
	bnez $v0, error_shift_row
	
	# validate direction value, $s4, valid value: 0,1
	bltz $s4, error_shift_row	# direction < 0
	li $t0, 1
	bgt $s4, $t0, error_shift_row	# direction > 1
	
	# get the begining address of row
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	move $a2, $s3			# row, i
	li $a3, 0			# col, j
	jal get_cell_addr
	move $s5, $v0			# s5 has starting adress of row
	# get last address of row
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	move $a2, $s3			# row, i
	addi $a3, $s2, -1		# j = m -1 
	jal get_cell_addr
	move $s6, $v0			# s6 has ending adress of row
	
	# save staring and end address before looping in any direction
	# move $s0, $s5			# start address 
	# move $s1, $s6			# end address 
	move $a0, $s5
	move $a1, $s6
	
	bnez $s4, direction_1_shift_row
	# direction value is 0
	jal shift_row_left
	j exit_shift_row
	
	# jump here when direction is one	
	direction_1_shift_row:
	jal shift_row_right
	j exit_shift_row
	
	error_shift_row:
		li $v0, -1
	exit_shift_row:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra
	############################################

shift_col:
    #int shift_col(cell[][] board, int num_rows, int num_cols, int col, int direction)
	############################################
	lw $t0, 0($sp)		# direction
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m 
	move $s3, $a3		# col		
	move $s4, $t0		# direction
	
	# validate n,m,col
	move $a0, $s1		# n
	move $a1, $s2		# m
	li  $a2, 0		# row
	move $a3, $s3		# col
	jal validate
	bnez $v0, error_shift_col
	
	# validate direction value, $s4, valid value: 0,1
	bltz $s4, error_shift_col	# direction < 0
	li $t0, 1
	bgt $s4, $t0, error_shift_col	# direction > 1
	
	# get the begining address of col
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	li  $a2, 0			# row, i
	move $a3, $s3			# col, j
	jal get_cell_addr
	move $s5, $v0			# s5 has starting adress of row
	# get last address of col
	move $a0, $s0			# base_addr
	move $a1, $s2			# m 
	addi $a2, $s1, -1		# row= n -1
	move $a3, $s3			# col
	jal get_cell_addr
	move $s6, $v0			# s6 has ending adress of row
	
	move $a0, $s5		# start addr
	move $a1, $s6		# end addr
	move $s7, $s2		# m
	sll $s7, $s7, 1		# 2m = jump
	
	bnez $s4, direction_1_shift_col
	# direction is 0
	move $a2, $s7		# jump, positive means going down
	jal shift_col_up
	j  exit_shift_col	
	
	direction_1_shift_col:
    		sub $a2, $0, $s7	# jump, negative means going up
    		jal shift_col_down
    		j  exit_shift_col
		
	# shifting col is finished
	    	
    	error_shift_col:
    		li $v0, -1
    	exit_shift_col:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra 
	############################################

check_state:
    #int check_state(cell[][] board, int num_rows, int num_cols)
	############################################
	
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m
	li $s4, 0		# cell counter, value >= 2
	mul $s5, $s1, $s2	# total cells = n * m 
	
	# find the max address possible, s4 = last address
	mul $t1, $s1, $s2	# n * m
	sll $t1, $t1, 1		# n * m * 2
	li $t2, 2
	sub $t1, $t1, $t2	# ( n * m * 2 ) - 2
	add $s3, $s0, $t1	# last address inclusive , base_addr + ( n * m * 2 ) - 2
	
	# check if any cell has value >= 2048
	move $t0, $s0		# current = start
	move $t1, $s3		# end
	li $t2, 2048		# check value
	won_loop_cs:
		bgt $t0, $t1, end_won_loop_cs	# current > end
		lh $t3, 0($t0)			# cell value
		bge $t3, $t2, game_won_cs	# cell value >= 2048
		addi $t0, $t0, 2		# current ++
		blez $t3, won_loop_cs		# cell value <=0, go next value
		addi $s4, $s4, 1
		j won_loop_cs
	end_won_loop_cs:
	
	# s4 has number of cells containing values
	# s5 has total values
	bne $s4, $s5, game_continue_cs		# not cells have values
	
	# all cells have value
	# need to check for movable step in row and col
	
	# row movable check
	li $s4, 0		# row value
	loop_row_movable_cs:
		beq $s4, $s1,end_loop_row_movable_cs	# i == n  
		# put starting addr into s6
		move $a0, $s0	# base
		move $a1, $s2	# m
		move $a2, $s4	# row
		li $a3, 0	# col = 0
		jal get_cell_addr
		move $s6, $v0
		
		# put ending addr into s7
		move $a0, $s0		# base
		move $a1, $s2		# m
		move $a2, $s4		# row
		addi $a3, $s2, -1	# col = m -1 
		jal get_cell_addr
		move $s7, $v0
		
		# call helper to check for movable row
		move $a0, $s6		# start 
		move $a1, $s7		# end
		jal check_movable_row
		li $t0, 1
		beq $v0, $t0,game_continue_cs	# valid step exist
		
		# no valid step in current row, go to next
		addi $s4, $s4, 1 
		j loop_row_movable_cs
	end_loop_row_movable_cs:
	
	# col movable check
	li $s4, 0		# col value
	sll $s5, $s2, 1		# 2m
	loop_col_movable_cs:
		beq $s4, $s2,end_loop_col_movable_cs	# i == m  
		# put starting addr into s6
		move $a0, $s0	# base
		move $a1, $s2	# m
		li $a2, 0	# row = 0
		move $a3, $s4	# col 
		jal get_cell_addr
		move $s6, $v0
		
		# put ending addr into s7
		move $a0, $s0		# base
		move $a1, $s2		# m
		addi $a2, $s1, -1	# row -1
		move $a3, $s4		# col 
		jal get_cell_addr
		move $s7, $v0
		
		# call helper to check for movable row
		move $a0, $s6		# start 
		move $a1, $s7		# end
		move $a2, $s5		# jump
		jal check_movable_col
		li $t0, 1
		beq $v0, $t0,game_continue_cs	# valid step exist
		
		# no valid step in current row, go to next
		addi $s4, $s4, 1 
		j loop_col_movable_cs
	end_loop_col_movable_cs:
	
	# there is no more movable steps
	li $v0, -1
	j exit_check_state
	
	game_continue_cs:
		li $v0, 0
		j exit_check_state
	
	game_won_cs:
		li $v0, 1
	
	exit_check_state:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra 
	############################################

user_move:
    #(int, int) user_move(cell[][] board, int num_rows, int num_cols, char dir)
	############################################
	# Preserve Data, All of them are beeing used to store arguments and runing variable
	addi $sp, $sp, -36				
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	# Store Arguments
	move $s0, $a0		# base address 		
	move $s1, $a1		# n
	move $s2, $a2		# m
	move $s3, $a3		# dir
	
	# check for direction values and choose the direction
	# lw $t0, string_L
	#li $t0, 0x0000004c
	li $t0, 'L'
	beq $s3, $t0, direction_left_um
	#lw $t0, string_R
	#li $t0, 0x00000052
	li $t0, 'R'
	beq $s3, $t0, direction_right_um
	#lw $t0, string_U
	#li $t0, 0x00000055
	li $t0, 'U'
	beq $s3, $t0, direction_up_um
	#lw $t0, string_D
	#li $t0, 0x00000044
	li $t0, 'D'
	beq $s3, $t0, direction_down_um
	# dir value is invalid
	j error_user_move
	
	
	######## Left Direction move
	direction_left_um:
		# shift left all rows, merge left all rows, shift left all rows
		li $s4, 0		# current row value
		li $s5, -1		# error value
		loop_dl_um:
			beq $s4, $s1, end_loop_dl_um	# current == total rows			
			# call shift left
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# row
			li $t0, 0		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_row
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call for merge row
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# row
			li $t0, 0		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal merge_row
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call shift left
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# row
			li $t0, 0		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_row
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# go to next row
			addi $s4, $s4, 1
			j loop_dl_um
		end_loop_dl_um:
		# all shifting is done, check state now 
		move $a0, $s0		# base 
		move $a1, $s1		# n
		move $a2, $s2		# m
		jal check_state
		move $v1, $v0
		li $v0, 0
		j exit_user_move 
	
	######## Right Direction move
	direction_right_um:
		# shift right all rows, merge right all rows, shift right all rows
		li $s4, 0		# current row value
		li $s5, -1		# error value
		loop_dr_um:
			beq $s4, $s1, end_loop_dr_um	# current == total rows			
			# call shift right
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# row
			li $t0, 1		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_row
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call for merge row
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# row
			li $t0, 1		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal merge_row
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call shift right
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# row
			li $t0, 1		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_row
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# go to next row
			addi $s4, $s4, 1
			j loop_dr_um
		end_loop_dr_um:
		# all shifting is done, check state now 
		move $a0, $s0		# base 
		move $a1, $s1		# n
		move $a2, $s2		# m
		jal check_state
		move $v1, $v0
		li $v0, 0
		j exit_user_move 
	
	######## Up Direction move
	direction_up_um:
		# shift up all col, merge up all col, shift up all rows
		li $s4, 0		# current col value
		li $s5, -1		# error value
		loop_du_um:
			beq $s4, $s2, end_loop_du_um	# current == total cols			
			# call shift col
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# col
			li $t0, 0		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_col
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call for merge col
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# col
			li $t0, 1		# dir, top to bottom
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal merge_col
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call shift col
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# col
			li $t0, 0		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_col
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# go to next row
			addi $s4, $s4, 1
			j loop_du_um
		end_loop_du_um:
		# all shifting is done, check state now 
		move $a0, $s0		# base 
		move $a1, $s1		# n
		move $a2, $s2		# m
		jal check_state
		move $v1, $v0
		li $v0, 0
		j exit_user_move 
	
	######## down Direction move
	direction_down_um:
		# shift down all col, merge down all col, shift down all rows
		li $s4, 0		# current col value
		li $s5, -1		# error value
		loop_dd_um:
			beq $s4, $s2, end_loop_dd_um	# current == total cols			
			# call shift col
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# col
			li $t0, 1		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_col
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call for merge col
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# col
			li $t0, 0		# dir, bottom to top
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal merge_col
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# call shift col
			move $a0, $s0		# base 
			move $a1, $s1		# n
			move $a2, $s2		# m 
			move $a3, $s4		# col
			li $t0, 1		# dir
			addi $sp, $sp, -4
			sw $t0, 0($sp)
			jal shift_col
			addi $sp, $sp, 4
			beq $v0, $s5, error_user_move	# check for error
			# go to next row
			addi $s4, $s4, 1
			j loop_dd_um
		end_loop_dd_um:
		# all shifting is done, check state now 
		move $a0, $s0		# base 
		move $a1, $s1		# n
		move $a2, $s2		# m
		jal check_state
		move $v1, $v0
		li $v0, 0
		j exit_user_move 
	
	
	error_user_move:
		li $v0, -1
		li $v1, -1
	exit_user_move:
		# Unpreserve Data				
		lw $s0, 0($sp)
		lw $s1, 4($sp)
		lw $s2, 8($sp)
		lw $s3, 12($sp)
		lw $s4, 16($sp)
		lw $s5, 20($sp)
		lw $s6, 24($sp)
		lw $s7, 28($sp)
		lw $ra, 32($sp)
		addi $sp, $sp, 36
		jr $ra 
	############################################
    
##############################
# HELPER FUNCTIONS
##############################

validate:
	# int validate(n,m,i,j)
	# zero for success and -1 for error
	li $t0, 2
	blt $a0, $t0, error_validate		# n < 2
	blt $a1, $t0, error_validate		# m < 2
	bltz $a2, error_validate		# i < 0
	bltz $a3, error_validate		# j < 0
	bge $a2, $a0, error_validate		# i >= n
	bge $a3, $a1, error_validate		# j >= m
	# All values are valid 
	li $v0, 0
	jr $ra 
	
	error_validate:
		li $v0, -1
		jr $ra 
		
get_cell_addr:
	# addr get_cell(base, m , i, j)
	# bytes are taken values of 2
	# addr = base_addr + elem_size_in_bytes * (i * num_columns + j)
	mul $t0, $a1, $a2		# i * num_columns
	add $t0, $t0, $a3		# (i * num_columns + j)
	li $t1, 2
	mul $t0, $t0, $t1		# 2 * (i * num_columns + j)
	add $v0, $t0, $a0		# bese_addr + 2 * (i * num_columns + j)
	jr $ra

merge:
	# int merge(addr1, addr2)
	# Merge the value at addr1 and addr2, addr1 has doubled value
	# 0 for Sucess and -1 for errors
	lh $t0, 0($a0)
	lh $t1, 0($a1)
	blez $t0, exit_merge
	blez $t1, exit_merge
	bne $t0, $t1, exit_merge
	
	sll $t0, $t0, 1			# double the value ar addr1
	li $t1, -1			# reset value at addr2
	sh $t0, 0($a0)
	sh $t1, 0($a1)
	
	exit_merge:
		jr $ra
		
shift_row_left:
	# int shift_row_left(addr1, addr2)
	# return the cells moved
	# used by shift_row and direction 0
	li $t0, 0
	li $t7, -1
	move $t1, $a0		# storing addr = start addr
	find_store_addr_srl:
		bgt $t1, $a1, exit_find_store_addr_srl	# store > end
		lh $t2, 0($t1)
		blez $t2, end_find_store_addr_srl
		addi $t1, $t1, 2
		j find_store_addr_srl
	end_find_store_addr_srl:
	# t1 has valid storing address
	addi $t2, $t1, 2		# t2 has check val
	shift_loop_srl:
		bgt $t2, $a1, exit_find_store_addr_srl
		lh $t3, 0($t2)
		blez $t3, iinc_shift_loop_srl 
		# t3 >= 2 and need to be shifted
		sh $t3, 0($t1)
		sh $t7, 0($t2)
		addi $t0, $t0, 1
		addi $t1, $t1, 2	# store addr ++ 
		iinc_shift_loop_srl:
			addi $t2, $t2, 2	# load addr ++
		j shift_loop_srl
		
	exit_find_store_addr_srl:
		move $v0, $t0
		jr $ra

shift_row_right:
	# int shift_row_right(addr1, addr2)
	# return the cells moved
	# used by shift_row and direction 0
	li $t0, 0
	li $t7, -1
	move $t1, $a1		# storing addr = end addr
	find_store_addr_srr:
		blt $t1, $a0, exit_find_store_addr_srr	# store < start
		lh $t2, 0($t1)
		blez $t2, end_find_store_addr_srr
		addi $t1, $t1, -2
		j find_store_addr_srr
	end_find_store_addr_srr:
	# t1 has valid storing address
	addi $t2, $t1, -2		# t2 has check val
	shift_loop_srr:
		blt $t2, $a0, exit_find_store_addr_srr
		lh $t3, 0($t2)
		blez $t3, iinc_shift_loop_srr 
		# t3 >= 2 and need to be shifted
		sh $t3, 0($t1)
		sh $t7, 0($t2)
		addi $t0, $t0, 1
		addi $t1, $t1, -2		# store addr -- 
		iinc_shift_loop_srr:
			addi $t2, $t2, -2	# load addr --
		j shift_loop_srr
		
	exit_find_store_addr_srr:
		move $v0, $t0
		jr $ra

shift_col_up:
	# int shift_col_up(addr1, addr2, jump)
	# return the cells moved
	# used by shift_col and direction 0
	li $t0, 0
	li $t7, -1
	move $t1, $a0		# storing addr = start addr
	find_store_addr_scu:
		bgt $t1, $a1, exit_find_store_addr_scu	# store > end
		lh $t2, 0($t1)
		blez $t2, end_find_store_addr_scu
		add $t1, $t1, $a2
		j find_store_addr_scu
	end_find_store_addr_scu:
	# t1 has valid storing address
	add $t2, $t1, $a2		# t2 has check val
	shift_loop_scu:
		bgt $t2, $a1, exit_find_store_addr_scu
		lh $t3, 0($t2)
		blez $t3, iinc_shift_loop_scu
		# t3 >= 2 and need to be shifted
		sh $t3, 0($t1)
		sh $t7, 0($t2)
		addi $t0, $t0, 1
		add $t1, $t1, $a2	# store addr ++ 
		iinc_shift_loop_scu:
			add $t2, $t2, $a2	# load addr ++
		j shift_loop_scu
		
	exit_find_store_addr_scu:
		move $v0, $t0
		jr $ra
		
shift_col_down:
	# int shift_col_down(addr1, addr2, jump)
	# need jump value in negative
	# return the cells moved
	# used by shift_col and direction 0
	li $t0, 0
	li $t7, -1
	move $t1, $a1		# storing addr = end addr
	find_store_addr_scd:
		blt $t1, $a0, exit_find_store_addr_scd	# store < start
		lh $t2, 0($t1)
		blez $t2, end_find_store_addr_scd
		add $t1, $t1, $a2
		j find_store_addr_scd
	end_find_store_addr_scd:
	# t1 has valid storing address
	add $t2, $t1, $a2		# t2 has check val
	shift_loop_scd:
		blt $t2, $a0, exit_find_store_addr_scd
		lh $t3, 0($t2)
		blez $t3, iinc_shift_loop_scd 
		# t3 >= 2 and need to be shifted
		sh $t3, 0($t1)
		sh $t7, 0($t2)
		addi $t0, $t0, 1
		add $t1, $t1, $a2		# store addr -- 
		iinc_shift_loop_scd:
			add $t2, $t2, $a2	# load addr --
		j shift_loop_scd
		
	exit_find_store_addr_scd:
		move $v0, $t0
		jr $ra
		
check_movable_row:
	# check_movable_row(addr1, addr2)
	# returns 1 if there is a movable step
	# otherwise 0 for not movable step
	move $t0, $a0		# start 
	addi $t1, $a0, 2	# next
	
	loop_cmr:
		bgt $t1, $a1,exit_loop_cmr	# adj addr > last
		lh $t2, 0($t0)
		lh $t3, 0($t1)
		beq $t2, $t3,  valid_move_possible_cmr	# both adj has same value
		addi $t0, $t0, 2
		addi $t1, $t1, 2
		j loop_cmr
	exit_loop_cmr:
	# row does not have same values as adj. So, not valid move possible
	li $v0, 0
	jr $ra
	
	valid_move_possible_cmr:
		li $v0, 1
		jr $ra
		
check_movable_col:
	# check_movable_col(addr1, addr2, jump)
	# returns 1 if there is a movable step
	# otherwise 0 for not movable step
	move $t0, $a0		# start 
	add $t1, $a0, $a2	# next
	
	loop_cmc:
		bgt $t1, $a1,exit_loop_cmc	# adj addr > last
		lh $t2, 0($t0)
		lh $t3, 0($t1)
		beq $t2, $t3,  valid_move_possible_cmc	# both adj has same value
		add $t0, $t0, $a2
		add $t1, $t1, $a2
		j loop_cmc
	exit_loop_cmc:
	# row does not have same values as adj. So, not valid move possible
	li $v0, 0
	jr $ra
	
	valid_move_possible_cmc:
		li $v0, 1
		jr $ra
		
#################################################################
# Student defined data section
#################################################################
.data
.align 2  # Align next items to word boundary
#place all data declarations here


